//
//  main.swift
//  proj1
//
//  Created by Filip Klembara on 17/02/2020.
//

import Foundation
import FiniteAutomata
import Simulator

// MARK: - Main
func main() -> Result<Void, RunError> {
    
    //An attempt to parse arguments. I would normally use the ArgumentParser library
    let arg = CommandLine.arguments
    if (arg.count < 3) {return .failure(.wrongArgument)}
        
    else {
        let path = arg[2]
        let fm = FileManager.default
        
        do {
            let fa = fm.contents(atPath: path)
            if fa == nil {
                return .failure(.fileError)
            }
            
            let f = try JSONDecoder().decode(FiniteAutomata.self, from: fa!)
            
            //checking automaton for undefined states or symbols before simulation
            for transition in f.transitions {
                if (!f.states.contains(transition.to))||(!f.states.contains(transition.from)){
                    return .failure(.undefinedState)
                }else if(!f.symbols.contains(transition.with)){
                    return .failure(.undefinedSymbol)
                }
            }
                
            let simulator = Simulator(finiteAutomata: f)
            let result = simulator.simulate(on: arg[1])
            
            if result == [] {
                return .failure(.notAccepted)
            }
            
            for char in result {
                print(char)
            }
        }catch{
            return .failure(.decodingError)
        }
        
    }
    return .success(())
    
}

// MARK: - program body
let result = main()

switch result {
case .success:
    break
case .failure(let error):
    var stderr = STDERRStream()
    print("Error:", error.description, to: &stderr)
    exit(Int32(error.code))
}

