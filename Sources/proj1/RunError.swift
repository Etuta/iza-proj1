//
//  RunError.swift
//  proj1
//
//  Created by Filip Klembara on 17/02/2020.
//

enum RunError: Error {

    case notAccepted
    case wrongArgument
    case fileError
    case decodingError
    case undefinedState
    case undefinedSymbol
    case otherErr
}

// MARK: - Return codes
extension RunError {
    var code: Int {
        switch self {

        case .notAccepted:
            return 6
        case .wrongArgument:
            return 11
        case .fileError:
            return 12
        case .decodingError:
            return 20
        case .undefinedState:
            return 21
        case .undefinedSymbol:
            return 22
        case .otherErr:
            return 99
        }
    }
}

// MARK:- Description of error
extension RunError: CustomStringConvertible {
    var description: String {
        switch self {
        case .notAccepted:
            return "Input not accepted by the automaton"
        case .wrongArgument:
            return "Wrong argument"
        case .fileError:
            return "Error occured while working with the file"
        case .decodingError:
            return "Error occured while decoding automaton"
        case .undefinedState:
            return "Automaton contains undefined state"
        case .undefinedSymbol:
            return "Automaton contains undefined symbol"
        case .otherErr:
            return "Undefined error occured"
        }
    }
}
