//
//  FiniteAutomata.swift
//  FiniteAutomata
//
//  Created by Filip Klembara on 17/02/2020.
//

/// Finite automata
public struct FiniteAutomata: Decodable  {
    
    public var states: [String]
    public var symbols: [String]
    public var initialState: String
    public var finalStates: [String]
    public var transitions: [Transition]
    
}


//extension FiniteAutomata: Decodable {
//}

public struct Transition: Codable {
    
    public var with: String
    public var to: String
    public var from: String

    
}
