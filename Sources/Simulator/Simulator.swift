//
//  Simulator.swift
//  Simulator
//
//  Created by Filip Klembara on 17/02/2020.
//

import FiniteAutomata

/// Simulator
public struct Simulator {
    /// Finite automata used in simulations
    private let finiteAutomata: FiniteAutomata

    /// Initialize simulator with given automata
    /// - Parameter finiteAutomata: finite automata
    public init(finiteAutomata: FiniteAutomata) {
        self.finiteAutomata = finiteAutomata
    }

    /// Simulate automata on given string
    /// - Parameter string: string with symbols separated by ','
    /// - Returns: Empty array if given string is not accepted by automata,
    ///     otherwise array of states
    public func simulate(on string: String) -> [String] {
        
        let arguments = string.split(separator: ",").map(String.init)
        let result = simulate_rec(input: arguments, position: 0, state: finiteAutomata.initialState)
        
        return result.reversed()
    }
    
    
    /// Recursive function to find all possible paths in a finite automaton on given string
    /// - Parameters:
    ///   - input: array of strings, containing symbols on which we simulate the automaton
    ///   - position: current index in arguments
    ///   - state: current state of the automaton
    /// - Returns: Array of states through which simulation ends in accepting state, otherwise empty array
    func simulate_rec(input: [String], position: Int, state: String) -> [String] {
                
        if (position >= input.count){
            if (finiteAutomata.finalStates.contains(state)){
                return [state]
            }
            else {
                return []
            }
        }
        
        for transition in finiteAutomata.transitions {
            if (transition.with == input[position])&&(transition.from == state){
                var result = simulate_rec(input: input, position: position+1, state: transition.to)
                if (result != []){
                    result.append(state)
                    return result
                    }
            }
            
        }        
        return []
    }
}
